# Imports
import requests
import logging
import threading
import time
import random
from concurrent.futures import ThreadPoolExecutor
import os, psutil
import matplotlib.pyplot as plt
import gc

def test(item):
    #random_time = random.randint(0,10)
    logging.info(f'Thread ID = {threading.get_ident()}')
    logging.info(f'Thread name = {threading.current_thread().name}')
    #time.sleep(random_time)
    list_number.append(item)
    logging.info(f'finished')

def downloader(list_number):
    items = 0
    while not close_the_single_thread:
        if(len(list_number) != 0):
            #logging.info(f'Current memory usage = {process.memory_info().rss}')
            
            # do not pick up too quick, to prevent memory exponential while creating worker threads in pool
            if(items >= workers):
                process_usage_from_single_thread.append(process.memory_info().rss)
                time_list.append(time.time() - start_time)
            
            response = requests.get("https://google.com")
            f = open("download_file.txt" , "a")
            f.write(response.text + "\n\n")
            f.close()
            del list_number[0]
            gc.collect()
            items = items + 1
            logging.info(f'Single thread ID = {threading.get_ident()}')
            logging.info(f'Single thread name = {threading.current_thread().name}')
            logging.info(f'Processing Item = {items}')

def main():
    logging.basicConfig(format='%(levelname)s - %(asctime)s: %(message)s', datefmt='%H:%M:%S' , level=logging.DEBUG)
    logging.info("Starting")
    global start_time
    start_time = time.time()

    # reset the file content
    f = open("download_file.txt" , "w")
    f.close()

    # queue
    global close_the_single_thread
    global list_number
    list_number = []
    close_the_single_thread = False
    # argument list_number
    single_thread = threading.Thread(target=downloader, args=(list_number,))
    single_thread.start()

    # 1 worker = 5 tasks to send to single thread
    global workers
    workers = 5
    items = workers * 5

    # set up memory starter here
    global process
    process = psutil.Process(os.getpid())
    
    global process_usage_from_single_thread
    global time_list
    process_usage_from_single_thread = []
    time_list = []
    #process_usage_from_single_thread.append(process.memory_info().rss)
    #time_list.append(0)

    # thread pool starts distributing work
    with ThreadPoolExecutor(max_workers=workers) as executor:
        executor.map(test, range(items))
        total_time_for_worker_threads_from_pool = time.time() - start_time
        while True:
            if(len(list_number) == 0):
                close_the_single_thread = True
                break
    
    single_thread.join()

    # plot graph
    plt.xlabel('x - time')
    plt.ylabel('y - memory usage')
    plt.plot(time_list,process_usage_from_single_thread)
    plt.show()

    logging.info(f'Total time for worker from threadpool = {total_time_for_worker_threads_from_pool}')
    logging.info(f'Total time for single thread = {time.time() - start_time}')
    logging.info('Finished')
    

if __name__ == "__main__":
    main()