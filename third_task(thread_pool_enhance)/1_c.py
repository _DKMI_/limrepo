# Imports
import requests
import logging
import time
from multiprocessing.pool import Pool

def downloader():
    response = requests.get("https://google.com")
    f = open("download_file.txt" , "a")
    f.write(response.text + "\n\n")
    f.close()

def main():
    logging.basicConfig(format='%(levelname)s - %(asctime)s: %(message)s', datefmt='%H:%M:%S' , level=logging.DEBUG)
    logging.info("Starting")
    global start_time
    start_time = time.time()

    # reset the file content
    f = open("download_file.txt" , "w")
    f.close()

    # process pool
    with Pool(4) as p:
        p.map(downloader(), range(20))
        total_time_for_worker_process_from_pool = time.time() - start_time

    logging.info(f'Total time for process to done = {total_time_for_worker_process_from_pool}')
    logging.info('Finished')
    

if __name__ == "__main__":
    main()