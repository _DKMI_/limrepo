# Imports
import requests
import logging
import threading
import time
from queue import Queue
from threading import Thread
from concurrent.futures import ThreadPoolExecutor

# Create thread class
class DownloadWorker(Thread):
    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            item = self.queue.get()
            try:
                downloader(item)
            finally:
                self.queue.task_done()

# This function is for data sender worker from thread pool
def test(item):
    logging.info(f'Thread ID = {threading.get_ident()}')
    logging.info(f'Thread name = {threading.current_thread().name}')
    queue_one.put(item)
    logging.info(f'finished')

# This function is for download threads
def downloader(item):
    response = requests.get("https://google.com")
    f = open("download_file.txt" , "a")
    f.write(response.text + "\n\n")
    f.close()
    logging.info(f'Download worker thread ID = {threading.get_ident()}')
    logging.info(f'Download worker thread name = {threading.current_thread().name}')

def main():
    logging.basicConfig(format='%(levelname)s - %(asctime)s: %(message)s', datefmt='%H:%M:%S' , level=logging.DEBUG)
    logging.info("Starting")
    global start_time
    start_time = time.time()

    # reset the file content
    f = open("download_file.txt" , "w")
    f.close()

    # queue
    global queue_one
    queue_one = Queue()

    # create download worker thread and start it
    global close_all_download_worker
    close_all_download_worker = False
    for x in range(20):
        download_worker = DownloadWorker(queue_one)
        download_worker.daemon = True
        download_worker.start()

    # 1 worker = 5 tasks to send to single thread
    workers = 10
    items = workers * 5

    # thread pool starts distributing work
    with ThreadPoolExecutor(max_workers=workers) as executor:
        executor.map(test, range(items))
        total_time_for_worker_threads_from_pool = time.time() - start_time

    queue_one.join()

    logging.info(f'Total time for worker from threadpool = {total_time_for_worker_threads_from_pool}')
    logging.info(f'Total time for download worker thread = {time.time() - start_time}')
    logging.info('Finished')
    

if __name__ == "__main__":
    main()