from bing_image_downloader import downloader

downloader.download("dog image" , limit=100 , output_dir="dog_images")
downloader.download("cat image" , limit=100 , output_dir="cat_images")
downloader.download("bird image" , limit=100 , output_dir="bird_images")
downloader.download("fish image" , limit=100 , output_dir="fish_images")