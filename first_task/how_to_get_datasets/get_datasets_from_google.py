from google_images_download import google_images_download

#class instantiation
response = google_images_download.googleimagesdownload()

#creating list of arguments
arguments = {"keywords":"fish","format": "jpg","print_urls":True}

#passing the arguments to the function
absolute_image_paths = response.download(arguments)

#printing absolute paths of the downloaded images
print(absolute_image_paths)