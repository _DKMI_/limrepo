import glob
import os
import json
from natsort import natsorted
from PIL import Image

#Get all files from a directory that have an extension jpg
files  = glob.glob("bird_images/bird/*.jpg")

json_list = []
#process each picture as per your api
for pic in files:
    jpgfile = Image.open(pic)
    filename = os.path.basename(jpgfile.filename)

    # python dict: single quote string
    temp_dict = {
        "image_name": filename,
        "tag_id": "bird"
    }
    # json: double quote string
    temp_json = json.dumps(temp_dict)
    json_list.append(temp_json)

# natural sort
json_list = natsorted(json_list)

for json in json_list:
    print(json)