from concurrent.futures.process import EXTRA_QUEUED_CALLS
import logging
import threading
import time
import random
from concurrent.futures import ThreadPoolExecutor

# for 3 threads from thread pool, we randomly get a value from 0 to 100
# we then sleep with a random time
# we then send the random value to fourth_thread
def random_number_generator(i):
    rng_sleep = random.randrange(1,3)
    rng_value = random.randrange(0,100)
    logging.info(f'Task {i} | Sleep for {rng_sleep} | Thread ID = {threading.get_ident()} | Thread name = {threading.current_thread().name}')
    time.sleep(rng_sleep)
    queue.append(rng_value)

# fourth_thread will decide if the value is odd or even
# if odd, incorrect
# if even, correct
def decision_function():
    while True:
        if(terminate_for_fourth_thread == True and queue == []):
            break
        elif(queue == []):
                time.sleep(10)

        while(queue != []):
            if(queue.pop() % 2 == 0):
                logging.info("Correct, value is even")
            else:
                logging.error("Incorrect, value is odd")

def main():
    logging.basicConfig(format='%(levelname)s - %(asctime)s: %(message)s', datefmt='%H:%M:%S' , level=logging.DEBUG)
    logging.info("Starting")

    # create fourth thread
    global queue
    global terminate_for_fourth_thread
    queue = []
    terminate_for_fourth_thread = False
    fourth_thread = threading.Thread(target=decision_function)
    fourth_thread.start()

    # create thread pool
    workers = 3

    with ThreadPoolExecutor(max_workers=workers) as executor:
        """
        for i in range(0, 201):
            executor.submit(random_number_generator, i)
        """
        i = 0
        while True:
            executor.submit(random_number_generator, i)
            i = i + 1

    terminate_for_fourth_thread = True

    # wait for fourth_thread to finish running its function(), then main will only continue running codes below
    # .join() must be here
    fourth_thread.join()

    logging.info('Finished')

if __name__ == "__main__":
    main()