import random
import logging
import time
import threading
from concurrent.futures import ThreadPoolExecutor

# Create my own event system
class Event:
    def __init__(self, event_id, event_name):
        self.event_id = event_id
        self.event_name = event_name
        self.func = None

    def make_thread_to_sleep(self):
        logging.info(f'Thread {threading.current_thread().name} with {self.event_name} is sleeping...')
        time.sleep(5)

    """
    def making_thread_to_wait(self):
        logging.info(f'Thread {threading.current_thread().name} with {self.event_name} is waiting...')
        threading.current_thread().wait()

    def wake_other_threads_up(self):
        time.sleep(5)
        logging.info(f'Thread {threading.current_thread().name} is notifying other threads...')
        threading.current_thread().notify_all()
    """

# Fruit sorter
def fruit_sorter(fruit , event_obj):
    logging.info(f'Thread {threading.current_thread().name} is active and sorting {fruit}')

    s = random.randrange(1,5)
    
    out_while_loop = False
    while True:
        for x in correct_list:
            if(x == fruit):
                for each in range(s):
                    basket.append(x)
                #event_obj.wake_other_threads_up()
                out_while_loop = True
                break
            elif(x not in basket):
                #event_obj.making_thread_to_wait()
                event_obj.make_thread_to_sleep()
                break
        
        if(out_while_loop == True):
            break
    


def main():

    logging.basicConfig(format="%(levelname)s - %(asctime)s: %(message)s" , datefmt="%H:%M:%S" , level=logging.DEBUG)
    logging.info(f'Start App')

    # create a task that put how many fruits needed for each category into a basket in specific sequence
    # [grapes, oranges, mangoes, bananas, pears] , must be in this order!!!
    global correct_list
    global basket
    basket = []
    correct_list = ['grapes', 'oranges', 'mangoes', 'bananas', 'pears']
    fruit_list = ['grapes', 'oranges', 'mangoes', 'bananas', 'pears']
    random.shuffle(fruit_list)
    logging.info(f'Getting random shuffle fruit list')

    # create Event objects
    event_list = []
    for i in range(1,6):
        event_list.append(Event(i , f'EVENT_GENERIC_000{i}'))
    logging.info(f'Creating event object')

    # Thread pool
    with ThreadPoolExecutor(max_workers=5) as executor:
        executor.map(fruit_sorter, fruit_list , event_list)

    print(fruit_list)
    print(basket)

    logging.info(f'Finish App')

if __name__ == "__main__":
    main()